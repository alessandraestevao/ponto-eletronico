package tech.mastertech.itau.ponto.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class JwtFilter extends OncePerRequestFilter {

  @Override 
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    JwtTokenProvider tokenProvider = new JwtTokenProvider();
    
    String token = parseToken(request);
    
    if(tokenProvider.validarToken(token)) {
      String userId = tokenProvider.lerToken(token);
      String role = tokenProvider.lerRole(token);

      System.out.println("ROLEZEIROD = " + userId);
      System.out.println("ROLEZEIRO = " + role);
      
      SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
      
      List<GrantedAuthority> authorities = new ArrayList<>();
      authorities.add(authority);
      
      UsernamePasswordAuthenticationToken auth = 
          new UsernamePasswordAuthenticationToken(userId, "", authorities);
      
      SecurityContextHolder.getContext().setAuthentication(auth);
    }
    
    filterChain.doFilter(request, response);
  }
  
  private String parseToken(HttpServletRequest req) {
    String bearerToken = req.getHeader("Authorization");
    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
        return bearerToken.substring(7, bearerToken.length());
    }
    return null;
}

}

package tech.mastertech.itau.ponto.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.ponto.models.Ponto;

public interface PontoRepository extends CrudRepository<Ponto, Integer>{

	@Query(value= "SELECT * FROM ponto WHERE colaborador_funcional = ?1 and DATE(data) = subdate(current_date, 1) order by data, hora", nativeQuery = true)
	Iterable<Ponto> findByAllFuncionalPontoDia(int funcional);

	@Query(value= "SELECT * FROM ponto WHERE colaborador_funcional = ?1 and DATE(data) <= subdate(current_date, 1) order by data, hora", nativeQuery = true)
	Iterable<Ponto> findByAllFuncionalPonto(int funcional);
	
	@Query(value= "SELECT * FROM ponto WHERE colaborador_funcional = ?1 and id = ?2", nativeQuery = true)
	Optional<Ponto> findByFuncionalPontoEspecifico(int funcional, int id);

	@Query(value= "SELECT * from ponto  where status = 1", nativeQuery = true)
	Iterable<Ponto> findAllByStatusPendente();
	
	@Query(value= "SELECT * FROM ponto WHERE id = ?1", nativeQuery = true)
	Optional<Ponto> findByIdPontoEspecifico(int id);


	
	
}

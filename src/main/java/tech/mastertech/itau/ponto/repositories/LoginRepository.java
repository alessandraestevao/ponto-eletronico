package tech.mastertech.itau.ponto.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.ponto.models.Login;

public interface LoginRepository extends CrudRepository<Login, Integer>{
	

}

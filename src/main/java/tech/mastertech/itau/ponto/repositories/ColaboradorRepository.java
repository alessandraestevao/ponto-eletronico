package tech.mastertech.itau.ponto.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.ponto.models.Colaborador;

public interface ColaboradorRepository extends CrudRepository<Colaborador, Integer>{

}

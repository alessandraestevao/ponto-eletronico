package tech.mastertech.itau.ponto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.models.Login;
import tech.mastertech.itau.ponto.repositories.ColaboradorRepository;

@Service
public class ColaboradorService {

	@Autowired
	private ColaboradorRepository colaboradorRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private LoginService loginService;

	public Colaborador setColaborador(Colaborador colaborador) {
		// Geração automática da senha (3 primeiros dígitos do CPF + 3 primeiras letras
		// do nome)
		String digitosCpf = colaborador.getCpf();
		String digitosNome = colaborador.getNome();
		String senha = digitosCpf.substring(0, 3) + digitosNome.substring(0, 3);

		Login login = new Login();

		login.setSenha(senha);
		login.setColaborador(colaborador);
		String senhaCriptografada = passwordEncoder.encode(login.getSenha());
		login.setSenha(senhaCriptografada);

		colaboradorRepository.save(colaborador);
		loginService.gerarLogin(login);

		return colaborador;
	}

	public Colaborador getColaborador(int funcional) {
		Optional<Colaborador> retorno = colaboradorRepository.findById(funcional);

		if (!retorno.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return retorno.get();
	}

	public Colaborador alterarColaborador(int funcional, Colaborador colaboradorAlter) {
		Colaborador colaboradorAntigo = getColaborador(funcional);
		colaboradorAntigo.setCargaHoraria(colaboradorAlter.getCargaHoraria());
		colaboradorAntigo.setNome(colaboradorAlter.getNome());
		colaboradorAntigo.setCargo(colaboradorAlter.getCargo());
		return colaboradorRepository.save(colaboradorAntigo);
	}

	public Iterable<Colaborador> getColaboradores() {
		return colaboradorRepository.findAll();
	}
}

package tech.mastertech.itau.ponto.services;

import java.text.ParseException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.ponto.exceptions.ValidacaoException;
import tech.mastertech.itau.ponto.enums.Status;
import tech.mastertech.itau.ponto.models.Ponto;
import tech.mastertech.itau.ponto.repositories.PontoRepository;

@Service
public class PontoService {

	@Autowired
	private PontoRepository pontoRepository;

	public Ponto criar(Ponto ponto) {

		return pontoRepository.save(ponto);
	}

	public Iterable<Ponto> consultaTodos(int funcional) {
		Iterable<Ponto> registros = null;
		registros = pontoRepository.findByAllFuncionalPonto(funcional);

		return registros;
	}

	public Iterable<Ponto> consultaPorDia(int funcional) throws ParseException {
		Iterable<Ponto> registros = null;
		registros = pontoRepository.findByAllFuncionalPontoDia(funcional);
		
		return registros;
	}
	
	
	

	public Ponto consultaDiaEspecifico(int funcional, int id) throws ParseException {
		Optional<Ponto> registros = pontoRepository.findByFuncionalPontoEspecifico(funcional, id);

		if (!registros.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Registro não encontrado");
		}

		return registros.get();
	}

	public Ponto consultaDiaEspecificoPorId(int id) throws ParseException {
		Optional<Ponto> registros = pontoRepository.findByIdPontoEspecifico(id);

		if (!registros.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Registro não encontrado");
		}

		return registros.get();
	}

	public String calculaTotalHoras(Iterable<Ponto> pontos) throws ParseException {

		List<LocalTime> listaEntrada = new ArrayList<LocalTime>();
		List<LocalTime> listaSaida = new ArrayList<LocalTime>();
		long deltahora = 0;
		long minutos = 0;
		long deltaminutos = 0;
		String total;

		for (Ponto ponto : pontos) {
			if (ponto.getTipoRegistro().name().equals("ENTRADA")) {
				listaEntrada.add(ponto.getHora());
			} else {
				listaSaida.add(ponto.getHora());
			}
		}
		
	
		
		

		if (listaEntrada.size() > 0 && listaSaida.size() > 0  && listaEntrada.size() == listaSaida.size() ) {
			for (int i = 0; i < listaEntrada.size(); i++) {
				deltahora += ChronoUnit.HOURS.between(listaEntrada.get(i), listaSaida.get(i));
				minutos += ChronoUnit.MINUTES.between(listaEntrada.get(i), listaSaida.get(i));
			}

			deltaminutos = minutos - (deltahora * 60);

			if (deltaminutos > 59) {
				deltahora += deltaminutos / 60;
				deltaminutos = minutos - (deltahora * 60);
			}

			total = deltahora + "h" + deltaminutos + "min";

		} else if (listaEntrada.size() == 0 && listaSaida.size() == 0) {
			total = "";

		} else {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Não foi possível calcular - Motivo: Número impar de marcações");
//			throw new ValidacaoException("Total Horas", "Não foi possível calcular - Motivo: Número impar de marcações");
		}
		
		return total;

	}

	public Ponto alterarPonto(int funcional, int id, LocalTime hora) throws ParseException {

		Ponto ponto = consultaDiaEspecifico(funcional, id);

		ponto.setHora(hora);
		ponto.setStatus(Status.PENDENTE);

		return pontoRepository.save(ponto);
	}

	public Iterable<Ponto> listarPonto() {
		Iterable<Ponto> registrosPendentes = null;
		registrosPendentes = pontoRepository.findAllByStatusPendente();
		return registrosPendentes;

	}

	public Ponto gerenciarPendencias(int id, int status) throws ParseException {
		Ponto ponto = consultaDiaEspecificoPorId(id);
		if (status == Status.APROVADO.getValor()) {
			ponto.setStatus(Status.APROVADO);

		} else {
			ponto.setStatus(Status.RECUSADO);
		}

		return pontoRepository.save(ponto);
	}

}

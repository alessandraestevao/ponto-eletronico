package tech.mastertech.itau.ponto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.ponto.models.Login;
import tech.mastertech.itau.ponto.models.RespostaLogin;
import tech.mastertech.itau.ponto.repositories.LoginRepository;
import tech.mastertech.itau.ponto.security.JwtTokenProvider;

@Service
public class LoginService {
	
	@Autowired
	private LoginRepository loginRepository;
	 
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public RespostaLogin loginColaborador(Login login){
		 		
		Optional<Login> optional = loginRepository.findById(login.getColaborador().getFuncional());
		
		if(optional.isPresent()) {
			Login dadosColaborador = optional.get();
			
			if (passwordEncoder.matches(login.getSenha(), dadosColaborador.getSenha())){
				System.out.println("senha ok");
				RespostaLogin respostaLogin = new RespostaLogin();
				respostaLogin.setToken(jwtTokenProvider.criarToken(dadosColaborador.getId()+"", dadosColaborador.getColaborador().getCargo()+""));
				respostaLogin.setColaborador(dadosColaborador.getColaborador());
				return respostaLogin;
			}
		}
		 	
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Colaborador não encontrado");
	}
	
	public Login gerarLogin (Login login) {
			
		return loginRepository.save(login);
	}
	
	
	

}

package tech.mastertech.itau.ponto.models;

import javax.validation.constraints.NotNull;

public class RetornoPonto {

	private Iterable<Ponto> ponto;
	@NotNull
	private String totalHoras;

	public Iterable<Ponto> getPonto() {
		return ponto;
	}

	public void setPonto(Iterable<Ponto> ponto) {
		this.ponto = ponto;
	}

	public String getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(String totalHoras) {
		this.totalHoras = totalHoras;
	}

}

package tech.mastertech.itau.ponto.models;

public class RespostaLogin {
	
	private Colaborador colaborador;
	private String token;
	
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

}

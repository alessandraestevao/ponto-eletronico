package tech.mastertech.itau.ponto.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import tech.mastertech.itau.ponto.enums.Cargo;

@Entity
public class Colaborador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer funcional;
	@NotNull
	private String nome;
	@CPF
	@NotNull
	private String cpf;
	@NotNull
	@Enumerated(EnumType.STRING)
	private Cargo cargo;
	private int cargaHoraria;
	@OneToMany
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Ponto> ponto;

	public Integer getFuncional() {
		return funcional;
	}
	public void setFuncional(Integer funcional) {
		this.funcional = funcional;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public List<Ponto> getPonto() {
		return ponto;
	}
	public void setPonto(List<Ponto> ponto) {
		this.ponto = ponto;
	}


}

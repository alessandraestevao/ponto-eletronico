package tech.mastertech.itau.ponto.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.services.ColaboradorService;

@CrossOrigin
@RestController
@RequestMapping("/ponto/colaborador")
public class ColaboradorController {
	
	@Autowired
	private ColaboradorService colaboradorService;
	
	@PostMapping("/cadastra")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Colaborador setColaborador(@Valid @RequestBody Colaborador colaborador) {
		return colaboradorService.setColaborador(colaborador);
	}
	
	@PatchMapping("/altera/{funcional}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public Colaborador alteraColaborador(@PathVariable int funcional, @RequestBody Colaborador colaboradorAlter) {
		return colaboradorService.alterarColaborador(funcional, colaboradorAlter);
	}
	
	@GetMapping("/busca")
	@ResponseStatus(code = HttpStatus.OK)
	public Iterable<Colaborador> getColaboradores(){
	    return colaboradorService.getColaboradores();
	  }

	@GetMapping("/busca/{funcional}")
	@ResponseStatus(code = HttpStatus.OK)
	public Colaborador getColaborador(@PathVariable int funcional){
	    return colaboradorService.getColaborador(funcional);
	  }

}

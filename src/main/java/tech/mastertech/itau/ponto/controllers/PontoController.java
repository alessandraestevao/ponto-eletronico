package tech.mastertech.itau.ponto.controllers;

import java.text.ParseException;
import java.time.LocalTime;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.ponto.enums.Status;
import tech.mastertech.itau.ponto.models.Ponto;
import tech.mastertech.itau.ponto.models.RetornoPonto;
import tech.mastertech.itau.ponto.services.PontoService;

@CrossOrigin
@RestController
@RequestMapping("/ponto")
public class PontoController {

	@Autowired
	private PontoService pontoService;

	private RetornoPonto retorno = new RetornoPonto();

	@PostMapping("/registrar")
	public Ponto criar(@RequestBody Ponto ponto) {

		return pontoService.criar(ponto);
	}

	@GetMapping("/consultar/{funcional}")
	public RetornoPonto consulta(@PathVariable int funcional, @RequestParam String tipo) throws ParseException {

		if (tipo.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		Iterable<Ponto> listaPontos = null;
		String totalHoras = null;
		if (tipo.equals("dia")) {
			listaPontos = pontoService.consultaPorDia(funcional);

		} else {
			listaPontos = pontoService.consultaTodos(funcional);
		}

		if (listaPontos == null || !listaPontos.iterator().hasNext()) {
			throw new ResponseStatusException(HttpStatus.NO_CONTENT);
		}

		totalHoras = pontoService.calculaTotalHoras(listaPontos);
		retorno.setPonto(listaPontos);
		retorno.setTotalHoras(totalHoras);

		return retorno;

	}

	@PatchMapping("/alterar/{funcional}/{id}")
	public Ponto alterarHora(@PathVariable int id, @PathVariable int funcional,
			@RequestBody HashMap<String, String> hora) throws ParseException {
		LocalTime horaCerta = LocalTime.parse(hora.get("hora"));
		if (horaCerta == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Deve informar uma hora");
		}

		return pontoService.alterarPonto(funcional, id, horaCerta);
	}

	@GetMapping("/pendentes")
	public Iterable<Ponto> consulta() {
		Iterable<Ponto> listaPontos = null;
		listaPontos = pontoService.listarPonto();

		if (listaPontos == null || !listaPontos.iterator().hasNext()) {
			throw new ResponseStatusException(HttpStatus.NO_CONTENT);
		}

		return listaPontos;

	}

	@PatchMapping("/gerenciarPendencias/{id}/{status}")
	public Ponto aprovarHora(@PathVariable int id, @PathVariable int status) throws ParseException {
		Ponto ponto = new Ponto();
		if (status == Status.APROVADO.getValor() || status == Status.RECUSADO.getValor()) {
			ponto = pontoService.gerenciarPendencias(id, status);

		}  else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Status invalido");

		}
	

		return ponto ;
	}

}

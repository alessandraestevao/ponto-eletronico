package tech.mastertech.itau.ponto.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.ponto.models.Login;
import tech.mastertech.itau.ponto.models.RespostaLogin;
import tech.mastertech.itau.ponto.services.LoginService;

@RestController
@RequestMapping("/login")
@CrossOrigin
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@PostMapping
	public RespostaLogin loginColaborador(@Valid @RequestBody Login login){
		return loginService.loginColaborador(login);
	}
	

}

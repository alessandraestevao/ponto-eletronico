package tech.mastertech.itau.ponto.enums;

public enum Status {
	
	CRIACAO(0),
	PENDENTE(1),
	APROVADO(2),
	RECUSADO(3);
	
	private int valor;
	
	private Status(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return valor;
	}
	
	
	

}

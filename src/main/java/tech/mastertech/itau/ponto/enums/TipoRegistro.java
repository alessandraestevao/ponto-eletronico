package tech.mastertech.itau.ponto.enums;

public enum TipoRegistro {
	
	ENTRADA,
	SAIDA;

}

package tech.mastertech.itau.ponto.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import tech.mastertech.itau.ponto.security.JwtFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.OPTIONS).permitAll()
        .antMatchers(HttpMethod.POST, "/login").permitAll()	
    	.antMatchers(HttpMethod.GET, "/ponto/pendentes").hasAuthority("COORDENADOR")
    	.antMatchers(HttpMethod.PATCH, "/ponto/gerenciarPendencias/*/**").hasAuthority("COORDENADOR")	
    	.antMatchers(HttpMethod.POST, "/ponto/colaborador/cadastra").permitAll()
        .antMatchers(HttpMethod.PATCH, "/ponto/colaborador/altera/**").hasAuthority("COORDENADOR")
        .antMatchers(HttpMethod.GET, "/ponto/colaborador/busca").hasAuthority("COORDENADOR")
        .antMatchers(HttpMethod.GET, "/ponto/colaborador/busca").hasAuthority("COORDENADOR")
        .anyRequest().authenticated()
        .and().addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class);
  } 

  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }
} 

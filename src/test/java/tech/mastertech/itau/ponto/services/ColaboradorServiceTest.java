package tech.mastertech.itau.ponto.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.ponto.enums.Cargo;
import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.models.Login;
import tech.mastertech.itau.ponto.repositories.ColaboradorRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ColaboradorService.class)
public class ColaboradorServiceTest {
	
	 @Autowired
	  private ColaboradorService sujeito;
	  
	  @MockBean
	  private ColaboradorRepository colaboradorRepository;
	  
	  @MockBean
	  private BCryptPasswordEncoder passwordEncoder;
	  
	  @MockBean
	  private LoginService loginService;

	  private Colaborador colaborador;
	  private String cpf = "30892029803";
	  private String senha = "123jos";
	  
	  @Before
	  public void preparar() {
	   
		colaborador = new Colaborador();
	    colaborador.setNome("José Cliente");
	    colaborador.setCpf(cpf);
	    colaborador.setCargaHoraria(8);
	    colaborador.setCargo(Cargo.ANALISTA);
	  }
	  
	  @Test
	  public void deveCriarUmColaborador() {
	    when(colaboradorRepository.save(colaborador)).thenReturn(colaborador);	   
	    when(loginService.gerarLogin(Mockito.any(Login.class))).thenReturn(new Login());	   
	    Colaborador colaboradorCriado = sujeito.setColaborador(colaborador);
	    
	    assertEquals(colaborador.getNome(), colaboradorCriado.getNome());
	    assertEquals(cpf, colaboradorCriado.getCpf());
	  }
	  
	  @Test
	  public void deveBuscarUmColaboradorPorFuncional() {
		    int funcional = 1;
		    colaborador.setFuncional(funcional);
		    
		    when(colaboradorRepository.findById(funcional)).thenReturn(Optional.of(colaborador));
		    
		    Colaborador colaboradorEncontrado = sujeito.getColaborador(funcional);
		    
		    assertEquals(colaborador.getCargaHoraria(), colaboradorEncontrado.getCargaHoraria());
		    assertEquals(colaborador.getCargo(), colaboradorEncontrado.getCargo());
		    assertEquals(colaborador.getCpf(), colaboradorEncontrado.getCpf());
		    assertEquals(colaborador.getNome(), colaboradorEncontrado.getNome());
	  }
  
	  
	  @Test
	  public void deveBuscarTodosOsColaboradoresCadastrados() {
	    when(colaboradorRepository.findAll()).thenReturn(Lists.list(colaborador));
	    
	    Iterable<Colaborador> colaboradoresIterable = sujeito.getColaboradores();
	    List<Colaborador> colaboradoresEncontrados = Lists.newArrayList(colaboradoresIterable);
	    
	    assertEquals(1, colaboradoresEncontrados.size());
	    assertEquals(colaborador, colaboradoresEncontrados.get(0));   
	  }
}

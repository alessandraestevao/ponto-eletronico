package tech.mastertech.itau.ponto.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.ponto.enums.Cargo;
import tech.mastertech.itau.ponto.enums.Status;
import tech.mastertech.itau.ponto.enums.TipoRegistro;
import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.models.Ponto;
import tech.mastertech.itau.ponto.repositories.PontoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PontoService.class)
public class PontoServiceTest {

	@Autowired
	private PontoService sujeito;

	@MockBean
	private PontoRepository pontoRepository;

	@Test
	public void deveConsultarTodosERetornaVazio() {
		
		Ponto ponto = new Ponto();
		Colaborador colaborador = new Colaborador();
		colaborador.setCpf("37137706894");
		colaborador.setCargo(Cargo.ANALISTA);
		colaborador.setCargaHoraria(8);
		colaborador.setFuncional(1234);
		colaborador.setNome("Alessandra");
		colaborador.setPonto(null);

	    String date = "30/05/2019";
		LocalDate inputDate = LocalDate.parse(date,DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalTime inputHour = LocalTime.parse("09:00");
		ponto.setData(inputDate);
		ponto.setHora(inputHour);
		ponto.setJustificativa("NDA");
		ponto.setId(1);
		ponto.setStatus(Status.CRIACAO);
		ponto.setTipoRegistro(TipoRegistro.ENTRADA);
		ponto.setColaborador(colaborador);

		Iterable<Ponto> pontoIterable = sujeito.consultaTodos(ponto.getColaborador().getFuncional());
		List<Ponto> pontoEncontrados = Lists.newArrayList(pontoIterable);
		
		Mockito.when(pontoRepository.findByAllFuncionalPonto(ponto.getColaborador().getFuncional())).thenReturn(Lists.list(ponto));
		assertEquals(0, pontoEncontrados.size());
		
		
	}

	
	
	
	
	@Test
	public void deveBaterPonto() {

		Ponto ponto = new Ponto();
		Colaborador colaborador = new Colaborador();
		colaborador.setCpf("37137706894");
		colaborador.setCargo(Cargo.ANALISTA);
		colaborador.setCargaHoraria(8);
		colaborador.setFuncional(1234);
		colaborador.setNome("Alessandra");
		colaborador.setPonto(null);

		String date = "30/05/2019";
		LocalDate inputDate = LocalDate.parse(date,DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalTime inputHour = LocalTime.parse("09:00");
		ponto.setData(inputDate);
		ponto.setHora(inputHour);
		ponto.setJustificativa("NDA");
		ponto.setId(1);
		ponto.setStatus(Status.CRIACAO);
		ponto.setTipoRegistro(TipoRegistro.ENTRADA);

		ponto.setColaborador(colaborador);

		Mockito.when(sujeito.criar(ponto)).thenReturn(ponto);
		Mockito.when(pontoRepository.save(any(Ponto.class))).then(argument -> argument.getArgument(0));

		Ponto pontoCriado = sujeito.criar(ponto);

		assertNotNull(pontoCriado.getId());
		assertNotNull(pontoCriado.getData());
		assertNotNull(pontoCriado.getHora());
		assertNotNull(pontoCriado.getColaborador());
		assertEquals(ponto, pontoCriado);
	}
	
	
	
	
	
	

}

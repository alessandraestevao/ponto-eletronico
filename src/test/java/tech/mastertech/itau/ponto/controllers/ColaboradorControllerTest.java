package tech.mastertech.itau.ponto.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.ponto.enums.Cargo;
import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.models.Login;
import tech.mastertech.itau.ponto.services.ColaboradorService;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ColaboradorController.class)
public class ColaboradorControllerTest {
	
	  @Autowired
	  private MockMvc mockMvc;
	  
	  @MockBean
	  private ColaboradorService colaboradorService;
	  
	  private ObjectMapper mapper = new ObjectMapper();
	  
	  private Colaborador colaborador;
	  private Login login;
	  private String cpf = "30892029803";
	  private String senha = "123jos";
	  
	  @Before
	  public void preparar() {
	    colaborador = new Colaborador();
	    colaborador.setFuncional(1234);
	    colaborador.setNome("José Funcionário");
	    colaborador.setCpf(cpf);
	    colaborador.setCargaHoraria(8);
	    colaborador.setCargo(Cargo.ANALISTA);
	  }
	  
	  @Test
	  @WithMockUser
	  public void deveCriarUmColaborador() throws Exception {
			 when(colaboradorService.setColaborador(any(Colaborador.class))).thenReturn(colaborador);    
			    
			    String colaboradorJson = mapper.writeValueAsString(colaborador);

			    mockMvc.perform(
			            post("/ponto/colaborador/cadastra")
			            .content(colaboradorJson)
			            .contentType(MediaType.APPLICATION_JSON_UTF8)
			           )
			          .andExpect(status().isCreated())
			          .andExpect(content().string(colaboradorJson));
	  }

}

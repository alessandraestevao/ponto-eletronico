package tech.mastertech.itau.ponto.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.ponto.enums.Cargo;
import tech.mastertech.itau.ponto.enums.Status;
import tech.mastertech.itau.ponto.enums.TipoRegistro;
import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.models.Ponto;
import tech.mastertech.itau.ponto.models.RetornoPonto;
import tech.mastertech.itau.ponto.services.PontoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PontoController.class)
public class PontoControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private PontoService pontoService;

	private Ponto ponto;
	private Colaborador colaborador;
	private RetornoPonto retorno;
	List<Ponto> registros;
	private int funcional = 123;
	private String totalHoras = "8";

	private ObjectMapper mapper = new ObjectMapper();
	
	 @Before
	  public void preparar() {
		LocalDate data = LocalDate.parse("30/05/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalTime hora = LocalTime.parse("12:50");
		
		colaborador = new Colaborador();
		colaborador.setFuncional(123);
		colaborador.setCpf("40317731882");
		colaborador.setCargo(Cargo.ANALISTA);
		colaborador.setCargaHoraria(8);
		colaborador.setNome("Francini");
		
		ponto = new Ponto();
		ponto.setId(1);
	    ponto.setData(data);
	    ponto.setHora(hora);
	    ponto.setColaborador(colaborador);
	    ponto.setTipoRegistro(TipoRegistro.ENTRADA);
	    ponto.setStatus(tech.mastertech.itau.ponto.enums.Status.CRIACAO);
	    ponto.setJustificativa("N/A");
	    
	    retorno = new RetornoPonto();
	    
	  }
	
	 @Test
	 @WithMockUser
	  public void deveRegistrarUmPonto() throws Exception {
	    when(pontoService.criar(any(Ponto.class))).thenReturn(ponto);
	    
	    String pontoJson = mapper.writeValueAsString(ponto);
	    
	    mockMvc.perform(
	        post("/ponto/registrar")
	        .content(pontoJson)
	        .contentType(MediaType.APPLICATION_JSON_UTF8)
	       )
	      .andExpect(status().isOk())
	      .andExpect(content().string(pontoJson));
	  }
	 
	 @Test
	 @WithMockUser
	  public void deveConsultarPontoDoDia() throws Exception {
		registros = Lists.list(ponto); 
		retorno.setTotalHoras("8");
	    retorno.setPonto(registros);
	    
		when(pontoService.consultaPorDia(123)).thenReturn(registros);
	    when(pontoService.calculaTotalHoras(registros)).thenReturn(totalHoras);
	    
	    mockMvc.perform(get("/ponto/consultar/" + funcional + "?tipo=dia"))
	            .andExpect(status().isOk())
	            .andExpect(content().string(mapper.writeValueAsString(retorno)));

	  }
	 
	 @Test
	 @WithMockUser
	  public void deveConsultarTodosOsRegistrosDePonto() throws Exception {
		registros = Lists.list(ponto); 
		retorno.setTotalHoras("8");
		retorno.setPonto(registros);
		    
	    when(pontoService.consultaTodos(123)).thenReturn(registros);
	    when(pontoService.calculaTotalHoras(registros)).thenReturn(totalHoras);
	    
	    mockMvc.perform(get("/ponto/consultar/" + funcional + "?tipo=todos"))
	            .andExpect(status().isOk())
	            .andExpect(content().string(mapper.writeValueAsString(retorno)));

	  }
	 
	 @Test
	 @WithMockUser
	  public void verificaSeTipoVeioVazio() throws Exception {
	    
	    mockMvc.perform(get("/ponto/consultar/" + funcional + "?tipo="))
	            .andExpect(status().isBadRequest());

	  }
	 
	 @Test
	 @WithMockUser
	  public void deveRetornarErroQuandoListaDePontosEstiverVazia() throws Exception {
	    when(pontoService.consultaTodos(123)).thenReturn(registros);
	    
	    mockMvc.perform(get("/ponto/consultar/" + funcional + "?tipo=todos"))
        .andExpect(status().isNoContent());

	  }
	 
//	 @Test
//	 @WithMockUser
//	 public void deveRetornarConsultaPontosPendentes() throws JsonProcessingException, Exception {
//		 
//		 
//		 ponto.setStatus(Status.PENDENTE);
//		 
//		 registros = Lists.list(ponto); 
//		 when(pontoService.listarPonto()).thenReturn(registros);
//		 
//		    
//		    mockMvc.perform(get("/ponto/pendentes"))
//		            .andExpect(status().isOk())
//		            .andExpect(content().string(mapper.writeValueAsString(registros)));
//
//	 }
//	 
 

}

package tech.mastertech.itau.ponto.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import javax.persistence.EnumType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.ponto.controllers.LoginController;
import tech.mastertech.itau.ponto.enums.Cargo;
import tech.mastertech.itau.ponto.models.Colaborador;
import tech.mastertech.itau.ponto.models.Login;
import tech.mastertech.itau.ponto.models.RespostaLogin;
import tech.mastertech.itau.ponto.security.JwtTokenProvider;
import tech.mastertech.itau.ponto.services.LoginService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LoginController.class)
public class LoginControllerTest {
	
	@Autowired
	private MockMvc mockmvc;
	
	@MockBean
	private LoginService loginService;
	
	@MockBean
	private JwtTokenProvider tokenProvider;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	private Login login = new Login();

	private Colaborador colaborador;
	private String senha = "1234";
	private String senhaCriptografada = "123123";
	private Login loginBanco = new Login();
	
	  @Before
	  public void preparar() {
		 colaborador = new Colaborador();
		 colaborador.setFuncional(1234);
		 colaborador.setNome("qualquer");
		 colaborador.setCpf("30892029803");
		 colaborador.setCargo(Cargo.ANALISTA);		 
		 login = new Login();
		 login.setSenha(senha);
		 login.setColaborador(colaborador);


	  }

	 
	@Test
	public void deveLogarUmCliente() throws Exception {
		login.setId(13);
	    
	    String token = "1234";
	    String idUsuario = String.valueOf(login.getId());
	    String role = "Analista";
//	    
	    Login login = new Login();
	    login.setColaborador(colaborador);
	    login.setSenha("1234");
	   
//	    
	    RespostaLogin resposta = new RespostaLogin();
	    resposta.setColaborador(colaborador);
	    resposta.setToken(token);
//	    
	    when(loginService.loginColaborador(any(Login.class))).thenReturn(resposta);

	    when(tokenProvider.criarToken(idUsuario, role)).thenReturn(token);
//	    
	    String loginJson = mapper.writeValueAsString(login);
	    String respostaJson = mapper.writeValueAsString(resposta);
//	    
	    mockmvc.perform(post("/login")
	            .contentType(MediaType.APPLICATION_JSON_UTF8)
	            .content(loginJson))
	        .andExpect(status().isOk())
	        .andExpect(content().string(containsString(respostaJson)))
	        .andExpect(content().string(containsString("token")));
	  }
}
